import './assets/scss/app.scss';
import 'bootstrap';
import $ from 'jquery';

let container = $('.first-column');
let image_container = $('.image');

$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: 'https://jsonplaceholder.typicode.com/photos',
        dataType: 'json',
        success: function (response) {
            response.forEach(function (data) {
                let $template = $('<div>', {class: 'item mb-3'}).append(
                    $('<p>',{text: data.title}),
                    $('<a>', {id: data.id, href: '#', text: data.thumbnailUrl})
                );
                $('.first-column').append($template);

                $('#' + data.id).click(function (a) {
                    a.preventDefault();
                    image_container.css({'background-image': 'url('+data.thumbnailUrl+')'});
                    image_container.find('img').attr("src", data.thumbnailUrl);
                })
            });
        }
    });
});


